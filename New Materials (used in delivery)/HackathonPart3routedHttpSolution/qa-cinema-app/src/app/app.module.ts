import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';
import { CinemaHeaderComponent } from './staticComponents/cinema-header/cinema-header.component';
import { CinemaFooterComponent } from './staticComponents/cinema-footer/cinema-footer.component';
import { CinemaHomeComponent } from './staticComponents/cinema-home/cinema-home.component';
import { CinemaScheduleModule } from './cinema-schedule/cinema-schedule.module';
import { CinemaScheduleComponent } from './cinema-schedule/cinema-schedule.component';
import { CinemaSignUpModule } from './cinema-sign-up/cinema-sign-up.module';
import { CinemaSignUpComponent } from './cinema-sign-up/cinema-sign-up.component';

const appRoutes: Routes = [
  {path: 'qacinema', component: CinemaHomeComponent},
  {path: 'schedule', component: CinemaScheduleComponent},
  {path: 'signup', component: CinemaSignUpComponent},
  {path: '', redirectTo: '/qacinema', pathMatch: 'full'},
  {path: '**', redirectTo: '/qacinema'}
];

@NgModule({
  declarations: [
    AppComponent,
    CinemaHeaderComponent,
    CinemaFooterComponent,
    CinemaHomeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    CinemaScheduleModule,
    CinemaSignUpModule,
    RouterModule.forRoot(appRoutes/*, {enableTracing: true}*/)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
