export class User {

    constructor(
        private _title: string,
        private _firstName: string,
        private _lastName: string,
        private _email: string,
        private _telephoneNumber?: string,
        private _dob?: string,
        private _gender?: string
    ) {}
}
