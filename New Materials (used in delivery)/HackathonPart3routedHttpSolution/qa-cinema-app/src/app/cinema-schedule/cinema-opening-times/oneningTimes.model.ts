export class OpeningTime {

    constructor(
        private _day: string, private _open: string, private _close: string
    ) {}

    public get day(): string {
        return this._day;
    }

    public get open(): string {
        return this._open;
    }

    public get close(): string {
        return this._close;
    }
}
