import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';

// import { OPENINGTIMES } from '../../../assets/openingTimes.js';
import { OpeningTime } from './oneningTimes.model.js';


@Injectable({
  providedIn: 'root'
})
export class OpeningTimesService {

  openingTimesUrl = `http://localhost:3000/openingTimes`;

  constructor(private http: HttpClient) { }

  getOpeningTimes(): Observable<OpeningTime[]> {
    return this.http.get<OpeningTime[]>(this.openingTimesUrl);
  }
}
