import { Component, OnInit } from '@angular/core';
import { OpeningTimesService } from './opening-times.service';
import { OpeningTime } from './oneningTimes.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-cinema-opening-times',
  templateUrl: './cinema-opening-times.component.html',
  styleUrls: ['./cinema-opening-times.component.css']
})
export class CinemaOpeningTimesComponent implements OnInit {

  private openingTimes: OpeningTime[];
  private displayAppError: Boolean = false;
  private displayServerError: Boolean = false;

  constructor(private openingTimesService: OpeningTimesService) { }

  ngOnInit() {
    this.getOpeningTimes();
  }

  getOpeningTimes(): void {
    this.openingTimesService.getOpeningTimes().subscribe(
      returnedOpeningTimes => {
        this.openingTimes = returnedOpeningTimes;
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          this.displayAppError = true;
        } else {
          this.displayServerError = true;
        }
      }
    );
  }

}
