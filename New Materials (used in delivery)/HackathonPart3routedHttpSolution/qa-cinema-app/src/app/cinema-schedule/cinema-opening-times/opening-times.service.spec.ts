import { TestBed } from '@angular/core/testing';

import { OpeningTimesService } from './opening-times.service';

describe('OpeningTimesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OpeningTimesService = TestBed.get(OpeningTimesService);
    expect(service).toBeTruthy();
  });
});
