import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

// import { FILMS } from '../../../assets/films';
import { Film } from './film.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WhatsonServiceService {

  private readonly filmsUrl = `http://localhost:3000/films`;

  constructor(private http: HttpClient) { }

  getFilms(): Observable<Film[]> {
    return this.http.get<Film[]>(this.filmsUrl);
  }
}
