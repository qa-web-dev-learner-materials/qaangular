import { Component, OnInit } from '@angular/core';
import { WhatsonServiceService } from './whatson-service.service';
import { Film } from './film.model';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-cinema-whatson',
  templateUrl: './cinema-whatson.component.html',
  styleUrls: ['./cinema-whatson.component.css']
})
export class CinemaWhatsonComponent implements OnInit {

  private films: Film[];
  private displayAppError: Boolean = false;
  private displayServerError: Boolean = false;

  constructor(private whatsonService: WhatsonServiceService) { }

  ngOnInit() {
    this.getFilms();
  }

  getFilms(): void {
    this.whatsonService.getFilms().subscribe(
      returnedFilms => {
        this.films = returnedFilms;
      },
      (err: HttpErrorResponse) => {
        if (err.error instanceof Error) {
          this.displayAppError = true;
        } else {
          this.displayServerError = true;
        }
      }
    );
  }
}
