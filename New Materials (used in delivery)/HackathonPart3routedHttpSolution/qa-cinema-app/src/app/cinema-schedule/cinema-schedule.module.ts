import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CinemaOpeningTimesComponent } from './cinema-opening-times/cinema-opening-times.component';
import { CinemaScheduleComponent } from './cinema-schedule.component';
import { CinemaWhatsonComponent } from './cinema-whatson/cinema-whatson.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CinemaOpeningTimesComponent, CinemaScheduleComponent, CinemaWhatsonComponent],
  exports: [CinemaScheduleComponent]
})
export class CinemaScheduleModule { }
