import { Film } from '../app/cinema-schedule/cinema-whatson/film.model';

const kot = new Film('01-Oct-2018 Monday', ['17:30', '20:00'], 'King Of Theives', 108, '15', './assets/KingOfThieves.jpg');
const tp = new Film('02-Oct-2018 Tuesday', ['17:30'], 'The Predator', 101, '15', './assets/ThePredator.jpg');
const hwc = new Film('02-Oct-2018 Tuesday', ['20:00'], 'The House with a Clock in its Walls', 105, '12A', './assets/TheHouse.jpg');

export const FILMS: Film[] = [
    kot,
    tp,
    hwc
];
