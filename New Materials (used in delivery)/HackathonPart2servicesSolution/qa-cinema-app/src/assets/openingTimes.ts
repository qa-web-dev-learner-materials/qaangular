import { OpeningTime } from "../app/cinema-schedule/cinema-opening-times/openingTimes.model";

const monThurs = new OpeningTime("Monday-Thursday", "17:30", "21:00");
const fri = new OpeningTime("Friday", "16:30", "22:00");
const sat = new OpeningTime("Saturday", "16:30", "23:00");
const sun = new OpeningTime("Sunday", "17:00", "21:30");

export const OPENINGTIMES: OpeningTime[] = [monThurs, fri, sat, sun];
