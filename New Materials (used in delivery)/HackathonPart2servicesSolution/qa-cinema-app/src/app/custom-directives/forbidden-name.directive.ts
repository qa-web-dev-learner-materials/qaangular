import { AbstractControl, ValidatorFn } from '@angular/forms';

export function forbiddenNameValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const nameRegEx = /^[a-zA-Z\'\-]+$/g;
    const forbidden = !nameRegEx.test(control.value);
    return forbidden ? {'forbiddenName': {value: control.value}} : null;
  };
}
