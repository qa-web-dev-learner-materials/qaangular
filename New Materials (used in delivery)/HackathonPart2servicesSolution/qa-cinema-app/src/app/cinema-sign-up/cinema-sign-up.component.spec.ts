import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CinemaSignUpComponent } from './cinema-sign-up.component';

describe('CinemaSignUpComponent', () => {
  let component: CinemaSignUpComponent;
  let fixture: ComponentFixture<CinemaSignUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CinemaSignUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CinemaSignUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
