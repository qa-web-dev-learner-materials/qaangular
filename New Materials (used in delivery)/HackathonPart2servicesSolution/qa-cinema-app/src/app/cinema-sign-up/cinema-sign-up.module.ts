import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { CinemaSignUpFormComponent } from './cinema-sign-up-form/cinema-sign-up-form.component';
import { CinemaSignUpComponent } from './cinema-sign-up.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [CinemaSignUpFormComponent, CinemaSignUpComponent],
  exports: [CinemaSignUpComponent]
})
export class CinemaSignUpModule { }
