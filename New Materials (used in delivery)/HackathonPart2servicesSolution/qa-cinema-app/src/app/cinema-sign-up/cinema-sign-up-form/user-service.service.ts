import { Injectable } from '@angular/core';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  users: User[] = [];

  constructor() { }

  addUser(user: User): void {
    const users = this.users;
    users.push(user);
    this.users = users;
    console.log(this.users);
    alert(`Thanks! Data added`);
  }
}
