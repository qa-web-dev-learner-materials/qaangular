import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";

import { forbiddenNameValidator } from "../../custom-directives/forbidden-name.directive";
import { forbiddenEmailValidator } from "src/app/custom-directives/forbidden-email.directive";
import { User } from "./user.model";
import { UserServiceService } from "./user-service.service";

@Component({
  selector: "app-cinema-sign-up-form",
  templateUrl: "./cinema-sign-up-form.component.html",
  styleUrls: ["./cinema-sign-up-form.component.css"]
})
export class CinemaSignUpFormComponent implements OnInit {
  private newUserForm: FormGroup;
  private newUser: User;
  private titles: { value: string; text: string }[] = [
    { value: "ms", text: "Ms" },
    { value: "miss", text: "Miss" },
    { value: "mrs", text: "Mrs" },
    { value: "mr", text: "Mr" },
    { value: "dr", text: "Dr" }
  ];

  private genders: { value: string; text: string }[] = [
    { value: "female", text: "Female" },
    { value: "male", text: "Male" }
  ];

  private firstNameValidity: {
    presence: Boolean;
    length: Boolean;
    chars: Boolean;
  };
  private lastNameValidity: {
    presence: Boolean;
    length: Boolean;
    chars: Boolean;
  };
  private emailValidity: { presence: Boolean; length: Boolean; chars: Boolean };

  constructor(
    private fb: FormBuilder,
    private userService: UserServiceService
  ) {}

  ngOnInit() {
    this.newUserForm = this.fb.group({
      title: ["", Validators.required],
      firstName: [
        "",
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(25),
          forbiddenNameValidator()
        ]
      ],
      lastName: [
        "",
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(25),
          forbiddenNameValidator()
        ]
      ],
      email: [
        "",
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(50),
          forbiddenEmailValidator()
        ]
      ],
      phoneNumber: [""],
      dob: [""],
      genderRadios: [""]
    });

    this.newUserForm.valueChanges.subscribe(() =>
      this.calculateFieldValidities()
    );

    this.firstNameValidity = {
      presence: false,
      length: false,
      chars: false
    };

    this.lastNameValidity = {
      presence: false,
      length: false,
      chars: false
    };

    this.emailValidity = {
      presence: false,
      length: false,
      chars: false
    };
  }

  calculateValidity(
    input
  ): { presence: Boolean; length: Boolean; chars: Boolean } {
    const presence: Boolean =
      input.touched &&
      input.errors &&
      input.errors.required &&
      input.errors.forbiddenName &&
      input.errors.forbiddenName.value === "";
    const chars: Boolean =
      input.touched &&
      input.errors &&
      input.errors.forbiddenName &&
      input.errors.forbiddenName.value !== "";
    const length: Boolean =
      input.touched && input.errors && input.errors.minlength;
    return { presence, length, chars };
  }

  calculateFieldValidities() {
    this.firstNameValidity = this.calculateValidity(
      this.newUserForm.get("firstName")
    );
    this.lastNameValidity = this.calculateValidity(
      this.newUserForm.get("lastName")
    );
    this.emailValidity = this.calculateValidity(this.newUserForm.get("email"));
  }

  prepareUser(user): User {
    return new User(
      user.title,
      user.firstName,
      user.lastName,
      user.email,
      user.phoneNumber,
      user.dob,
      user.gender
    );
  }

  onSubmit = evt => {
    console.log(`Form submitted with: `);
    console.log(this.newUserForm.value);
    evt.preventDefault();
    this.newUser = this.prepareUser(this.newUserForm.value);
    this.userService.addUser(this.newUser);
  };
}
