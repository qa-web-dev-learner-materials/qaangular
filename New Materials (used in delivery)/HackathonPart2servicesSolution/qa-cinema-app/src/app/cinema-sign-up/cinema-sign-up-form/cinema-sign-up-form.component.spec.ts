import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CinemaSignUpFormComponent } from './cinema-sign-up-form.component';

describe('CinemaSignUpFormComponent', () => {
  let component: CinemaSignUpFormComponent;
  let fixture: ComponentFixture<CinemaSignUpFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CinemaSignUpFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CinemaSignUpFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
