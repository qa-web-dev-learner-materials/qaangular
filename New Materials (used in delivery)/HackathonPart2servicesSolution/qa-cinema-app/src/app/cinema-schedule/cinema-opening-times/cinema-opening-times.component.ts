import { Component, OnInit } from "@angular/core";
import { OpeningTimesService } from "./opening-times.service";
import { OpeningTime } from "./openingTimes.model";

@Component({
  selector: "app-cinema-opening-times",
  templateUrl: "./cinema-opening-times.component.html",
  styleUrls: ["./cinema-opening-times.component.css"]
})
export class CinemaOpeningTimesComponent implements OnInit {
  private openingTimes: OpeningTime[];

  constructor(private openingTimesService: OpeningTimesService) {}

  ngOnInit() {
    this.getOpeningTimes();
  }

  getOpeningTimes(): void {
    this.openingTimesService
      .getOpeningTimes()
      .subscribe(openingTimes => (this.openingTimes = openingTimes));
  }
}
