import { Injectable } from "@angular/core";

import { Observable, of } from "rxjs";

import { OPENINGS } from "../../../assets/cinemaInfo.js";
import { OpeningTime } from "./openingTimes.model.js";

@Injectable({
  providedIn: "root"
})
export class OpeningTimesService {
  constructor() {}

  getOpeningTimes(): Observable<OpeningTime[]> {
    return of(OPENINGS);
  }
}
