export class OpeningTime {
  constructor(private _day: string, private _times: string) {}

  public get day(): string {
    return this._day;
  }

  public get times(): string {
    return this._times;
  }
}
