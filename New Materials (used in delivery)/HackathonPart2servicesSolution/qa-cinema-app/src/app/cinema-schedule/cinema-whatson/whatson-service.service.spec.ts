import { TestBed } from '@angular/core/testing';

import { WhatsonServiceService } from './whatson-service.service';

describe('WhatsonServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WhatsonServiceService = TestBed.get(WhatsonServiceService);
    expect(service).toBeTruthy();
  });
});
