import { Injectable } from "@angular/core";

import { Observable, of } from "rxjs";

import { FILMS } from "../../../assets/cinemaInfo.js";
import { Film } from "./film.model";

@Injectable({
  providedIn: "root"
})
export class WhatsonServiceService {
  constructor() {}

  getFilms(): Observable<Film[]> {
    return of(FILMS);
  }
}
