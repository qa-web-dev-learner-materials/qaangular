import { Component, OnInit } from '@angular/core';
import { WhatsonServiceService } from './whatson-service.service';
import { Film } from './film.model';

@Component({
  selector: 'app-cinema-whatson',
  templateUrl: './cinema-whatson.component.html',
  styleUrls: ['./cinema-whatson.component.css']
})
export class CinemaWhatsonComponent implements OnInit {

  private films: Film[];

  constructor(private whatsonService: WhatsonServiceService) { }

  ngOnInit() {
    this.getFilms();
  }

  getFilms(): void {
    this.whatsonService.getFilms()
      .subscribe(films => this.films = films);
  }

}
