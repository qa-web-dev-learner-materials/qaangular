export class Film {

    constructor(
        private _date: string,
        private _time: string[],
        private _title: string,
        private _runtime: number,
        private _rating: string,
        private _imgUrl: string
    ) {}

    get date(): string {
        return this._date;
    }

    get time(): string[] {
        return this._time;
    }

    get title(): string {
        return this._title;
    }

    get runtime(): number {
        return this._runtime;
    }

    get rating(): string {
        return this._rating;
    }

    get imgUrl(): string {
        return this._imgUrl;
    }
}
