import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CinemaWhatsonComponent } from './cinema-whatson.component';

describe('CinemaWhatsonComponent', () => {
  let component: CinemaWhatsonComponent;
  let fixture: ComponentFixture<CinemaWhatsonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CinemaWhatsonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CinemaWhatsonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
