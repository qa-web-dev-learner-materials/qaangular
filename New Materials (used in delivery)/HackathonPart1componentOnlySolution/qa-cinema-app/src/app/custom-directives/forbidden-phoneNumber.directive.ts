import { AbstractControl, ValidatorFn } from "@angular/forms";

export function forbiddenPhoneNumberValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const phoneNumberRegEx: RegExp = /^((\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/;
    const forbidden = control.value
      ? !phoneNumberRegEx.test(control.value)
      : false;
    return forbidden ? { forbidden: { value: control.value } } : null;
  };
}
