import { Component, OnInit } from "@angular/core";

import { FILMS } from "../../../assets/cinemaInfo.js";
@Component({
  selector: "app-cinema-whatson",
  templateUrl: "./cinema-whatson.component.html",
  styleUrls: ["./cinema-whatson.component.css"]
})
export class CinemaWhatsonComponent implements OnInit {
  films: [object] = FILMS;

  constructor() {}

  ngOnInit() {}
}
