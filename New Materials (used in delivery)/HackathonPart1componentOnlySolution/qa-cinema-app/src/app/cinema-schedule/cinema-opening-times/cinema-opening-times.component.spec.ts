import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CinemaOpeningTimesComponent } from './cinema-opening-times.component';

describe('CinemaOpeningTimesComponent', () => {
  let component: CinemaOpeningTimesComponent;
  let fixture: ComponentFixture<CinemaOpeningTimesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CinemaOpeningTimesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CinemaOpeningTimesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
