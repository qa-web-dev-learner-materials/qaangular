import { Component, OnInit } from '@angular/core';

import { OPENINGS } from '../../../assets/cinemaInfo.js';

@Component({
  selector: 'app-cinema-opening-times',
  templateUrl: './cinema-opening-times.component.html',
  styleUrls: ['./cinema-opening-times.component.css']
})
export class CinemaOpeningTimesComponent implements OnInit {

  openings: [string] = OPENINGS;

  constructor() { }

  ngOnInit() {
  }

}
