import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CinemaHeaderComponent } from './staticComponents/cinema-header/cinema-header.component';
import { CinemaFooterComponent } from './staticComponents/cinema-footer/cinema-footer.component';
import { CinemaHomeComponent } from './staticComponents/cinema-home/cinema-home.component';
import { CinemaScheduleModule } from './cinema-schedule/cinema-schedule.module';
import { CinemaSignUpModule } from './cinema-sign-up/cinema-sign-up.module';

@NgModule({
  declarations: [
    AppComponent,
    CinemaHeaderComponent,
    CinemaFooterComponent,
    CinemaHomeComponent,
  ],
  imports: [
    BrowserModule,
    CinemaScheduleModule,
    CinemaSignUpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
