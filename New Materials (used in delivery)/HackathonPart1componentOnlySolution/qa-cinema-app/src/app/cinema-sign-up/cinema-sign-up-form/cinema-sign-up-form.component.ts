import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";

import { forbiddenNameValidator } from "../../custom-directives/forbidden-name.directive";
import { forbiddenEmailValidator } from "../../custom-directives/forbidden-email.directive";
import { forbiddenPhoneNumberValidator } from "../../custom-directives/forbidden-phoneNumber.directive";

@Component({
  selector: "app-cinema-sign-up-form",
  templateUrl: "./cinema-sign-up-form.component.html",
  styleUrls: ["./cinema-sign-up-form.component.css"]
})
export class CinemaSignUpFormComponent implements OnInit {
  public newUser: FormGroup;
  public titles: { value: string; text: string }[] = [
    { value: "ms", text: "Ms" },
    { value: "miss", text: "Miss" },
    { value: "mrs", text: "Mrs" },
    { value: "mr", text: "Mr" },
    { value: "dr", text: "Dr" }
  ];

  public genders: { value: string; text: string }[] = [
    { value: "female", text: "Female" },
    { value: "male", text: "Male" }
  ];

  public firstNameValidity: {
    presence: Boolean;
    length: Boolean;
    chars: Boolean;
    status: string;
  };
  public lastNameValidity: {
    presence: Boolean;
    length: Boolean;
    chars: Boolean;
  };
  public emailValidity: { presence: Boolean; length: Boolean; chars: Boolean };

  public phoneNumberValidity: {
    presence: Boolean;
    length: Boolean;
    chars: Boolean;
  };

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.newUser = this.fb.group({
      title: [``, Validators.required],
      firstName: [
        ``,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(25),
          forbiddenNameValidator()
        ]
      ],
      lastName: [
        ``,
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(25),
          forbiddenNameValidator()
        ]
      ],
      email: [
        ``,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(50),
          forbiddenEmailValidator()
        ]
      ],
      phoneNumber: [``, [forbiddenPhoneNumberValidator()]],
      dob: [``],
      genderRadios: [``]
    });

    this.newUser.valueChanges.subscribe(() => this.calculateFieldValidities());

    this.firstNameValidity = {
      presence: false,
      length: false,
      chars: false,
      status: "INVALID"
    };

    this.lastNameValidity = {
      presence: false,
      length: false,
      chars: false
    };

    this.emailValidity = {
      presence: false,
      length: false,
      chars: false
    };

    this.phoneNumberValidity = {
      presence: false,
      length: false,
      chars: false
    };
  }

  calculateValidity(
    input
  ): { presence: Boolean; length: Boolean; chars: Boolean; status: string } {
    let presence: Boolean;
    let chars: Boolean;
    let length: Boolean;
    let status: string;
    if (!input.root.value.phoneNumber) {
      presence = input.touched && input.errors && input.errors.required;
      chars =
        input.touched &&
        input.errors &&
        input.errors.forbidden &&
        input.errors.forbidden.value !== ``;
      length = input.touched && input.errors && input.errors.minlength;
      status = input.status;
    } else {
      presence =
        input.touched &&
        input.value.length > 0 &&
        input.errors &&
        input.errors.forbidden &&
        input.errors.forbidden.value !== ``;
      length = input.value.length > 10;
      chars = !(
        input.errors &&
        input.errors.forbidden &&
        input.errors.forbidden.value !== ``
      );
      status = input.status;
    }
    return { presence, length, chars, status };
  }

  calculateFieldValidities() {
    this.firstNameValidity = this.calculateValidity(
      this.newUser.get(`firstName`)
    );
    this.lastNameValidity = this.calculateValidity(
      this.newUser.get(`lastName`)
    );
    this.emailValidity = this.calculateValidity(this.newUser.get(`email`));

    if (this.newUser.get(`phoneNumber`)) {
      this.phoneNumberValidity = this.calculateValidity(
        this.newUser.get(`phoneNumber`)
      );
    }
  }

  onSubmit = evt => {
    console.log(`Form submitted with: `);
    console.log(this.newUser.value);
  };
}
