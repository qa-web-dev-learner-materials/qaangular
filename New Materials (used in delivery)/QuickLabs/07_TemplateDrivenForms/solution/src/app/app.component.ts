import { Component } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Template-Driven Forms';

  genders = ['Female', 'Male'];

  submitted = false;

  user: User = new User();

  onSubmit() {
    this.submitted = true;
    console.log(this.user);
  }
}
