export class User {
    constructor(
        public firstname: string = '',
        public surname: string = '',
        public age: number = null,
        public gender: string = ''
    ) { }
}
