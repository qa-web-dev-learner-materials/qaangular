import { WordSeparatorPipe } from './word-separator.pipe';

describe('WordSeparatorPipe', () => {
  const pipe = new WordSeparatorPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it(`transforms any word but "Observables" by putting a space character after the word`, () => {
    expect(pipe.transform(`word`)).toBe(`word `);
  });

  it(`transforms the word "Observables" by putting an exclamation mark after it`, () => {
    expect(pipe.transform(`Observables`)).toBe(`Observables!`);
  });
});
