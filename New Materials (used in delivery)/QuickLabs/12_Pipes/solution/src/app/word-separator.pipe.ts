import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'wordSeparator'
})
export class WordSeparatorPipe implements PipeTransform {

  transform(word: string): string {
    if (word === `Observables`) {
      return `${word}!`;
    }
    return `${word} `;
  }

}
