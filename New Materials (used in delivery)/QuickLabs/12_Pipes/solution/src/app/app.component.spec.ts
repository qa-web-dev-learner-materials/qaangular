import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { WordSeparatorPipe } from './word-separator.pipe';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, WordSeparatorPipe
      ],
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
  }));

  it(`should add spaces to all words apart from "Observables" in the list`, async(() => {
    fixture.whenStable().then(() => {
      const hostElement = fixture.nativeElement;
      const wordSpans = hostElement.querySelectorAll('p span');

      fixture.detectChanges();

      for (const text of wordSpans) {
        if (text.textContent.indexOf(`Observables`) === -1) {
          expect(text.textContent.charAt(text.textContent.length - 1)).toBe(` `);
        } else {
          expect(text.textContent.charAt(text.textContent.length - 1)).toBe(`!`);
        }
      }
    });
  }));

  //   it('should create the app', () => {
  //     
  //     const app = fixture.debugElement.componentInstance;
  //     expect(app).toBeTruthy();
  //   });

  //   it(`should have as title 'solution'`, () => {
  //     const fixture = TestBed.createComponent(AppComponent);
  //     const app = fixture.debugElement.componentInstance;
  //     expect(app.title).toEqual('solution');
  //   });

  //   it('should render title in a h1 tag', () => {
  //     const fixture = TestBed.createComponent(AppComponent);
  //     fixture.detectChanges();
  //     const compiled = fixture.debugElement.nativeElement;
  //     expect(compiled.querySelector('h1').textContent).toContain('Welcome to solution!');
  //   });
});
