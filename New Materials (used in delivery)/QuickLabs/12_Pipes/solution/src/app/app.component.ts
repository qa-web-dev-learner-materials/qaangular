import { Component, OnInit } from '@angular/core';

import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  today = new Date();

  words$: Observable<string[]> = of([`Pipes`, `can`, `be`, `asynchronous`, `impure`, `and`, `used`, `with`, `Observables`]);

}
