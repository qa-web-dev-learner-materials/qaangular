import { ageRangeValidator } from './age-range-validator.directive';
import { AbstractControl, ValidationErrors } from '@angular/forms';

describe('min and max age range validator tests', () => {
    const ageRangeValidatorFunction = ageRangeValidator(10, 20);
    const control: { value: any } = { value: '' };
    let result: ValidationErrors;

    it(`should return an object if an empty string or alphanumeric string is tested`, () => {
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toEqual({ 'ageRange': { value: control.value } });
        control.value = `Arbitrary String 1234`;
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toEqual({ 'ageRange': { value: control.value } });
    });

    it(`should return an object if a value of 9 or less OR 21 or more is supplied (as string or number)`, () => {
        control.value = '9';
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toEqual({ 'ageRange': { value: control.value } });
        control.value = 21;
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toEqual({ 'ageRange': { value: control.value } });
    });

    it(`should return null if a value of 10 or more AND 20 or LESS is supplied (as string or number)`, () => {
        control.value = '11';
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toBeNull();
        control.value = 19;
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toBeNull();
    });
});
