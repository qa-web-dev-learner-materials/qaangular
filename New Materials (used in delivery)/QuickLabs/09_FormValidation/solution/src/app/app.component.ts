import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ageRangeValidator } from './age-range-validator.directive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Reactive Forms';
  genders = ['Female', 'Male'];

  constructor(private fb: FormBuilder) { }

  userForm = this.fb.group({
    firstname: ['', [Validators.required, Validators.minLength(2)]],
    surname: ['', [Validators.required, Validators.minLength(2)]],
    address: this.fb.group({
      houseNo: [''],
      street: [''],
      city: [''],
      postcode: ['']
    }),
    // Section 1
    // age: [null, Validators.min(18)],
    // Section 2 - using a Custom validator
    age: [null, ageRangeValidator(18, 68)],
    gender: ['']
  });
}
