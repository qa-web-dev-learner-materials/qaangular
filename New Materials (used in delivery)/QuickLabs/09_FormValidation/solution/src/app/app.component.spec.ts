import { TestBed, async, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('AppComponent', () => {
    let fixture: ComponentFixture<AppComponent>;
    let app: AppComponent;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            imports: [ReactiveFormsModule]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        app = fixture.debugElement.componentInstance;
        fixture.detectChanges();
    });

    it('should create the app', () => {
        // const fixture = TestBed.createComponent(AppComponent);
        // const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    // it(`should have as title 'solution'`, () => {
    //     const fixture = TestBed.createComponent(AppComponent);
    //     const app = fixture.debugElement.componentInstance;
    //     expect(app.title).toEqual('solution');
    // });

    // it('should render title in a h1 tag', () => {
    //     const fixture = TestBed.createComponent(AppComponent);
    //     fixture.detectChanges();
    //     const compiled = fixture.debugElement.nativeElement;
    //     expect(compiled.querySelector('h1').textContent).toContain('Welcome to solution!');
    // });

    describe(`Test the validity of the required fields`, () => {
        describe(`firstname field:`, () => {

            it(`should be initially invalid and have required error`, () => {
                const firstname = app.userForm.get('firstname');
                expect(firstname.valid).toBe(false);
                expect(firstname.errors['required']).toBe(true);
            });

            it(`should be valid if value has 2 or more characters`, () => {
                const firstname = app.userForm.get('firstname');
                firstname.setValue(`A`);
                expect(firstname.valid).toBe(false);
                expect(firstname.hasError('minlength')).toBe(true);
                firstname.setValue(`Ab`);
                expect(firstname.valid).toBe(true);
            });
        });
    });

    describe(`Test form validity`, () => {
        it(`should be valid when required fields are valid`, () => {
            expect(app.userForm.valid).toBe(false);
            app.userForm.get('firstname').setValue(`TestFirstName`);
            app.userForm.get('surname').setValue(`TestSurname`);
            app.userForm.get('age').setValue(30);
            expect(app.userForm.valid).toBe(true);
        });

        // Bonus solution test - when is the form invalid?
        it(`should be invalid when age has invalid value`, () => {
            expect(app.userForm.valid).toBe(false);
            app.userForm.get('firstname').setValue(`TestFirstName`);
            app.userForm.get('surname').setValue(`TestSurname`);
            app.userForm.get('age').setValue(12);
            expect(app.userForm.valid).toBe(false);
        });
    });
});
