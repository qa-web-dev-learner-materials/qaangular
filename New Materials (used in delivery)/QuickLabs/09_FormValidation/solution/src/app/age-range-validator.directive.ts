import { AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export function ageRangeValidator(min: number, max: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors => {
    const forbidden = ((isNaN(control.value) || !control.value) || control.value < min || control.value > max);
    return forbidden ? { 'ageRange': { value: control.value } } : null;
  };
}
