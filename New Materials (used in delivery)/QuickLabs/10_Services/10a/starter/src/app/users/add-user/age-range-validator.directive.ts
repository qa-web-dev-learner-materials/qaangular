import { AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';

export function ageRangeValidator(min: number, max: number): ValidatorFn {
  return (control: AbstractControl): ValidationErrors => {
    if ((!isNaN(control.value) && control.value) && control.value > min && control.value < max) {
      return { 'ageRange': true };
    }
    return null;
  };
}
