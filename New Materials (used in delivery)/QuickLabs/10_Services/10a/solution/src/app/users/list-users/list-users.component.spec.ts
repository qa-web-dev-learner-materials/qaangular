import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUsersComponent } from './list-users.component';
import { UserService } from '../user.service';

class MockUserService {
  getUsers = () => [{ firstname: `Test1`, surname: `Test1`, age: 25, gender: `female` }];
}

describe('ListUsersComponent', () => {
  let component: ListUsersComponent;
  let fixture: ComponentFixture<ListUsersComponent>;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ListUsersComponent],
      providers: [
        ListUsersComponent,
        { provide: UserService, useClass: MockUserService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListUsersComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should make a call to the service's getUsers method`, () => {
    userService.getUsers = jasmine.createSpy(`getUsers spy`);
    component.ngOnInit();
    expect(userService.getUsers).toHaveBeenCalled();
  });

  it(`should set users in the component to the return from the service `, () => {
    const expectedUsers = [{ firstname: `Test1`, surname: `Test1`, age: 25, gender: `female` }];
    component.ngOnInit();
    expect(component.users).toEqual(expectedUsers);
  });
});
