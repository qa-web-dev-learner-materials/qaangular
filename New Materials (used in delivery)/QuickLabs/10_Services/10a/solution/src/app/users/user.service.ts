import { Injectable } from '@angular/core';

import { User } from './user.model';
import { users as externalUserData } from './users.data';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private users: User[] = externalUserData;

  constructor() { }

  getUsers(): User[] {
    return this.users;
  }

  addUser(user: User): void {
    const users = this.users;
    users.push(user);
    this.users = users;
  }
}
