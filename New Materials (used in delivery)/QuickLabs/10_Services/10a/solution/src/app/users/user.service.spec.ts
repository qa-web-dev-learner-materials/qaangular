import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { User } from './user.model';
import { users as expectedUsers } from './users.data';

describe('UserService', () => {
  let service: UserService;
  const newUser: User = {
    firstname: `Bruce`,
    surname: `Wayne`,
    age: 33,
    gender: `male`
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});

    service = TestBed.get(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it(`getUsers() should return the 4 expected users`, () => {
    expect(service.getUsers()).toEqual(expectedUsers);
    expect(service.getUsers().length).toBe(4);
  });

  it(`addUser() should add the newUser to the users array`, () => {
    service.addUser(newUser);
    expect(service.getUsers()).toEqual(expectedUsers);
    expect(service.getUsers().length).toBe(5);
  });
});
