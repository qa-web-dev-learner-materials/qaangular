import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddUserComponent } from './add-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../user.service';

class MockUserService {
  addUser = user => null;
}

describe('AddUserComponent', () => {
  let component: AddUserComponent;
  let fixture: ComponentFixture<AddUserComponent>;
  let userService: UserService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddUserComponent],
      imports: [ReactiveFormsModule],
      providers: [
        AddUserComponent,
        { provide: UserService, useClass: MockUserService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddUserComponent);
    component = fixture.componentInstance;
    userService = TestBed.get(UserService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe(`Form submission tests:`, () => {
    const firstname = `Test2`;
    const surname = `Test2`;
    const age = 30;
    const gender = `male`;
    const expectedUser = { firstname, surname, age, gender };

    beforeEach(() => {
      component.userForm.get('firstname').setValue(firstname);
      component.userForm.get('surname').setValue(surname);
      component.userForm.get('age').setValue(age);
      component.userForm.get('gender').setValue(gender);
      fixture.detectChanges();
    });

    it(`should have a form value of the supplied user`, () => {
      expect(component.userForm.value).toEqual(expectedUser);
    });

    it(`should call addUser with the expectedUser`, () => {
      userService.addUser = jasmine.createSpy(`addUser spy`);
      component.onSubmit();
      expect(userService.addUser).toHaveBeenCalledTimes(1);
      expect(userService.addUser).toHaveBeenCalledWith(expectedUser);
    });
  });
});
