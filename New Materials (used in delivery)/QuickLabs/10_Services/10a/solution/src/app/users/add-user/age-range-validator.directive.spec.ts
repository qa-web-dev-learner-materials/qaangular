import { ageRangeValidator } from './age-range-validator.directive';
import { AbstractControl, ValidationErrors } from '@angular/forms';

describe('min and max age range validator tests', () => {
    const ageRangeValidatorFunction = ageRangeValidator(10, 20);
    const control: { value: any } = { value: '' };
    let result: ValidationErrors;

    it(`should return null if an empty string or alphanumeric string is tested`, () => {
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toBeNull();
        control.value = `Arbitrary String 1234`;
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toBeNull();
    });

    it(`should return null if a value of 10 or less OR 20 or more is supplied (as string or number)`, () => {
        control.value = '10';
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toBeNull();
        control.value = 20;
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toBeNull();
    });

    it(`should return true if a value of 10 or more AND 20 or LESS is supplied (as string or number)`, () => {
        control.value = '11';
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toEqual({ ageRange: true });
        control.value = 19;
        result = ageRangeValidatorFunction(control as AbstractControl);
        expect(result).toEqual({ ageRange: true });
    });
});
