import { User } from './user.model';

export const users: User[] = [
    {
        firstname: 'Steve',
        surname: 'Rogers',
        age: 102,
        gender: 'Male'
    },
    {
        firstname: 'Natasha',
        surname: 'Romanoff',
        age: 32,
        gender: 'Female'
    },
    {
        firstname: 'Tony',
        surname: 'Stark',
        age: 47,
        gender: 'Male'
    },
    {
        firstname: 'Carol',
        surname: 'Danvers',
        age: 62,
        gender: 'Female'
    },
];
