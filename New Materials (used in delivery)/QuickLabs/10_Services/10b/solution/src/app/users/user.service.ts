import { Injectable } from '@angular/core';

import { User } from './user.model';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly USERSURL = `http://localhost:3000/users`;

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.USERSURL);
  }


  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.USERSURL, user);
  }
}
