import { Component, OnInit } from '@angular/core';

import { User } from '../user.model';
import { UserService } from '../user.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  private users: User[];
  private error = false;
  private loading = true;
  private errorText: string;

  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.userService.getUsers().subscribe(
      data => {
        console.log(data);
        this.users = <User[]>data;
        this.loading = false;
      },
      (err: HttpErrorResponse) => {
        this.error = true;
        this.loading = false;
        if (err instanceof Error) {
          this.errorText = `An error occurred in the application.`;
        } else {
          this.errorText = `A server error occurred.`;
        }
      });
  }

}
