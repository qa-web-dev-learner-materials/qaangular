export class User {
    public firstname: string;
    public surname: string;
    public age: number;
    public gender: string;
    public id?: number;
}
