import { TestBed } from '@angular/core/testing';

import { UserService } from './user.service';
import { User } from './user.model';

import { users as expectedUsers } from './users.data';
import { asyncData, asyncError } from '../../testing/async-observable-helpers';
import { HttpErrorResponse, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

// describe('UserService', () => {
//   beforeEach(() => TestBed.configureTestingModule({}));

//   it('should be created', () => {
//     const service: UserService = TestBed.get(UserService);
//     expect(service).toBeTruthy();
//   });
// });
const newUser: User = {
  firstname: `Bruce`,
  surname: `Wayne`,
  age: 33,
  gender: `male`
};

describe(`Testing the service makes correct calls and gets/sends correct data`, () => {
  let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy };
  let userService: UserService;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    userService = new UserService(<any>httpClientSpy);
  });

  it(`should return the expected data when getAllUsers is called - once and only once`, () => {
    httpClientSpy.get.and.returnValue(asyncData(expectedUsers));

    userService.getUsers().subscribe(
      values => expect(values).toEqual(expectedUsers, `expected users`),
      fail
    );
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
  });

  it(`should return an object when addUser is called, calling with the supplied object - once and only once`, () => {

    httpClientSpy.post.and.returnValue(asyncData(newUser));

    userService.addUser(newUser).subscribe(
      value => expect(value).toEqual(newUser),
      fail
    );
    expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
  });

  it(`should return an error when the server returns a 404`, () => {
    const errorResponse = new HttpErrorResponse({
      error: `test 404 error`,
      status: 404,
      statusText: `Not Found`
    });

    httpClientSpy.get.and.returnValue(asyncError(errorResponse));

    userService.getUsers().subscribe(
      values => fail(`expected an error, not data`),
      error => expect(error.error).toContain(`test 404 error`)
    );

    httpClientSpy.post.and.returnValue(asyncError(errorResponse));

    userService.addUser(newUser).subscribe(
      values => fail(`expected an error, not data`),
      error => expect(error.error).toContain(`test 404 error`)
    );
  });
});

describe(`Mocking the HttpClient calls - ensuring correct locations are activated`, () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        UserService
      ]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    userService = TestBed.get(UserService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it(`should return an error when a 404 is returned`, () => {
    const errmsg = `generated 404 error`;
    const testUrl = `getMeA404`;

    httpClient.get<any[]>(testUrl).subscribe(
      data => fail(`fail with 404 error`),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(404, `status`);
        expect(error.error).toEqual(errmsg, `message`);
      }
    );

    const req = httpTestingController.expectOne(testUrl);

    req.flush(errmsg, { status: 404, statusText: `Not Found` });
  });

  describe(`Testing getUsers()`, () => {

    it(`should return expected data (called once)`, () => {
      userService.getUsers().subscribe(
        data => expect(data).toEqual(expectedUsers, `expected Users`),
        fail
      );

      const req = httpTestingController.expectOne(`${userService.USERSURL}`);
    });
  });

  describe(`Test addUser()`, () => {
    it(`should return the expected user on success (called once)`, () => {
      userService.addUser(newUser).subscribe(
        data => expect(data).toEqual(newUser, `should return the passed user`),
        fail
      );

      const req = httpTestingController.expectOne(`${userService.USERSURL}`);
    });
  });
});
