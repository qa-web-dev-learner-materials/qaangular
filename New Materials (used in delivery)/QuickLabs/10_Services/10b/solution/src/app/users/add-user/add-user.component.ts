import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { ageRangeValidator } from './age-range-validator.directive';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {

  private genders = ['Female', 'Male'];
  private addUserError = false;

  constructor(private fb: FormBuilder, private userService: UserService) { }

  userForm = this.fb.group({
    firstname: ['', [Validators.required, Validators.minLength(2)]],
    surname: ['', [Validators.required, Validators.minLength(2)]],
    age: [null, ageRangeValidator(18, 68)],
    gender: ['']
  });

  onSubmit(): void {
    const user: User = this.userForm.value;
    this.userService.addUser(user).subscribe(
      (response) => { },
      (err: HttpErrorResponse) => {
        this.addUserError = true;
        console.log(this.addUserError);
      }
    );
  }

}
