import { Injectable } from '@angular/core';
import { User } from './user.model';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  users: User[];

  constructor() { }

  getUsers() {
    return this.users;
  }

  addUser(user: User) {
    const users = this.users;
    users.push(user);
    this.users = users;
  }
}
