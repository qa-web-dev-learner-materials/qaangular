import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { ageRangeValidator } from './age-range-validator.directive';
import { User } from '../user.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent {

  genders = ['Female', 'Male'];

  constructor(private fb: FormBuilder, private userService: UserService) { }

  userForm = this.fb.group({
    firstname: ['', [Validators.required, Validators.minLength(2)]],
    surname: ['', [Validators.required, Validators.minLength(2)]],
    age: [null, ageRangeValidator(18, 68)],
    gender: ['']
  });

  onSubmit(): void {
    const user: User = this.userForm.value;
    this.userService.addUser(user);
  }

}
