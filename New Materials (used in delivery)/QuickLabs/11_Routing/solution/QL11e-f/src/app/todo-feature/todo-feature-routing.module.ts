import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoComponent } from './todo.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoDetailComponent } from './todo-detail/todo-detail.component';
import { TodoHomeComponent } from './todo-home/todo-home.component';

const todoRoutes: Routes = [
  {
    path: 'todo',
    component: TodoComponent,
    children: [
      {
        path: '',
        component: TodoListComponent,
        children: [
          {
            path: 'tododetails/:id',  // /:id added as part of Part f
            component: TodoDetailComponent
          },
          {
            path: '',
            component: TodoHomeComponent
          },
          {
            path: '**',
            component: TodoHomeComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(todoRoutes)
  ],
  exports: [RouterModule]
})
export class TodoFeatureRoutingModule { }
