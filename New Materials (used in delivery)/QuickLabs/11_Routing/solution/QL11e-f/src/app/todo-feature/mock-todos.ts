import { Todo } from './todo.model';

export const TODOS: Todo[] = [
    { id: 1, todoDetail: `Wash the dishes` },
    { id: 2, todoDetail: `Do a big shop` },
    { id: 3, todoDetail: `Change the light bulb in the hall` },
    { id: 4, todoDetail: `Tidy the children's bedrooms` }
];
