import { Component, OnInit } from '@angular/core';
import { Todo } from '../todo.model';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { TodoService } from '../todo.service';

import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';


@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.css']
})
export class TodoDetailComponent implements OnInit {
  todo$: Observable<Todo>;

  constructor(private todoService: TodoService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.todo$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => (
        this.todoService.getTodo(params.get('id'))
      ))
    );
  }

  goToToDoHome() {
    this.router.navigate(['/todo']);
  }

}
