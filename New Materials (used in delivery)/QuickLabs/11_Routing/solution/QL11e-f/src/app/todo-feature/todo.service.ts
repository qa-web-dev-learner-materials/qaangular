import { Injectable } from '@angular/core';
import { Todo } from './todo.model';
import { TODOS } from './mock-todos';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  constructor() { }

  getAll(): Observable<Todo[]> {
    return of(TODOS);
  }

  getTodo(todoId: number | string) {
    return this.getAll().pipe(
      map((todos: Todo[]) => todos.find(todo => todo.id === +todoId))
    );
  }
}
