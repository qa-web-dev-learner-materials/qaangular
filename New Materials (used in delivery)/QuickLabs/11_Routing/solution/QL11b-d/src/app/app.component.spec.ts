import { TestBed, async, fakeAsync, tick, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { routes } from './app-routing.module';
import { Page1Component } from './page1/page1.component';
import { Page2Component } from './page2/page2.component';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

describe('AppComponent', () => {
  let location: Location;
  let router: Router;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes)
      ],
      declarations: [
        AppComponent,
        Page1Component,
        Page2Component,
        PageNotFoundComponent
      ],
    }).compileComponents();
    router = TestBed.get(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(AppComponent);
  }));

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'solution'`, () => {
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('solution');
  });

  it(`navigate to "" redirects you to /page1`, fakeAsync(() => {
    router.navigate(['']);
    tick();
    expect(location.path()).toBe('/page1');
  }));

  it(`navigate to "/page1" takes you to /page1`, fakeAsync(() => {
    router.navigate(['/page1']);
    tick();
    expect(location.path()).toBe('/page1');
  }));

  it(`navigate to "/page2" takes you to /page2`, fakeAsync(() => {
    router.navigate(['/page2']);
    tick();
    expect(location.path()).toBe('/page2');
  }));
});
