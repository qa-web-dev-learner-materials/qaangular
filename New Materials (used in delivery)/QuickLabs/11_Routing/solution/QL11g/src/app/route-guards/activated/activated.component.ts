import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { CanComponentDeactivate } from '../activation.service';

@Component({
  selector: 'app-activated',
  templateUrl: './activated.component.html',
  styleUrls: ['./activated.component.css']
})
export class ActivatedComponent implements OnInit, CanComponentDeactivate {

  private title: string;
  private sub: any;

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.sub = this.route
      .data
      .subscribe(value => this.title = value.title);
  }

  canDeactivate(): boolean {
    return window.confirm(`Do you want to navigate away from this route? i.e return true for CanDeactivate`);
  }

}
