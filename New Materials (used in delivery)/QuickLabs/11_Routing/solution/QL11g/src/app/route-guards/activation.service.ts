import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanDeactivate } from '@angular/router';

import { Observable } from 'rxjs';

// the guard needn't know implementation details of components
// this CanDeactivate guard simply checks the component has the expected method
export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
  providedIn: 'root'
})
export class ActivationService implements CanActivate, CanActivateChild, CanDeactivate<CanComponentDeactivate> {

  constructor() { }

  canActivate(): boolean {
    return window.confirm(`Do you want to return true for CanActivate?`);
  }

  canActivateChild(): boolean {
    return window.confirm(`Do you want to return true for CanActivateChild?`);
  }

  canDeactivate(component: CanComponentDeactivate): Observable<boolean> | Promise<boolean> | boolean {
    if (!component) { return true; }
    return component.canDeactivate ? component.canDeactivate() : true;
  }
}
