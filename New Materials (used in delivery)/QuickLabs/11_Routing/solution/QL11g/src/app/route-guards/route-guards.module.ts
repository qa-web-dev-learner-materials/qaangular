import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouteGuardsRoutingModule } from './route-guards-routing.module';
import { RouteGuardsComponent } from './route-guards.component';
import { ActivatedComponent } from './activated/activated.component';

@NgModule({
  declarations: [RouteGuardsComponent, ActivatedComponent],
  imports: [
    CommonModule,
    RouteGuardsRoutingModule
  ]
})
export class RouteGuardsModule { }
