import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteGuardsComponent } from './route-guards.component';
import { ActivatedComponent } from './activated/activated.component';
import { ActivationService } from './activation.service';

const routes: Routes = [
  {
    path: 'routeguards',
    component: RouteGuardsComponent,
    canActivateChild: [ActivationService],
    children: [
      {
        path: 'canactivate',
        component: ActivatedComponent,
        data: { title: `CanActivate` },
        canActivate: [ActivationService]
      },
      {
        path: 'candeactivate',
        component: ActivatedComponent,
        data: { title: `CanDeactivate` },
        canDeactivate: [ActivationService]
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RouteGuardsRoutingModule { }
