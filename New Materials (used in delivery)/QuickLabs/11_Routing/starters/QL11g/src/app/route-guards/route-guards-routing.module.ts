import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteGuardsComponent } from './route-guards.component';
import { ActivatedComponent } from './activated/activated.component';

const routes: Routes = [
  {
    path: 'routeguards',
    component: RouteGuardsComponent,
    children: [
      {
        path: 'canactivate',
        component: ActivatedComponent,
        data: { title: `CanActivate` }
      },
      {
        path: 'candeactivate',
        component: ActivatedComponent,
        data: { title: `CanDeactivate` }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RouteGuardsRoutingModule { }
