import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TodoComponent } from './todo.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoDetailComponent } from './todo-detail/todo-detail.component';
import { TodoHomeComponent } from './todo-home/todo-home.component';

@NgModule({
  declarations: [TodoComponent, TodoListComponent, TodoDetailComponent, TodoHomeComponent],
  imports: [
    CommonModule
  ]
})
export class TodoFeatureModule { }
