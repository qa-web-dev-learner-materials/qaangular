import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-voter',
  templateUrl: './voter.component.html',
  styleUrls: ['./voter.component.css']
})
export class VoterComponent implements OnInit {

  @Input() upvotes: number;
  @Input() downvotes: number;

  @Output() vote = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  upvote() {
    this.vote.emit(1);
  }

  downvote() {
    this.vote.emit(-1);
  }

}
