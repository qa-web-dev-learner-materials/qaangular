import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  upvotes = 0;
  downvotes = 0;

  handleVote(vote) {
    vote === 1 ? this.upvotes += vote : this.downvotes -= vote;
  }

}
