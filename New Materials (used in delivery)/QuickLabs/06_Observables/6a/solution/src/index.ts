import { Observable, of, fromEvent, interval, timer } from 'rxjs';
import { filter, take, concat, map, switchAll } from 'rxjs/operators';

// of Operator
let creationOperatorObservable: Observable<number> = of(1, 2, 3, 4, 5);
creationOperatorObservable.subscribe((value: number) => console.log(`of emitted: ${value}`));

// fromEvent Operator
let fromEventButton: HTMLInputElement = document.querySelector(`#fromEvent`);
let fromEventOperatorObservable: Observable<Event> = fromEvent(fromEventButton, 'click');
fromEventOperatorObservable.subscribe((event: Event) => console.log(`The ${(<HTMLInputElement>event.target).id} button raised a ${event.type} event`));

// filter
let isEven: Observable<number> = of(0, 1, 2, 3, 4, 5);
isEven.pipe(
    filter((value: number) => value % 2 === 0)
).subscribe((value: number) => console.log(`Filtered - evens only: ${value}`));

// take
let takeFive: Observable<number> = of(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
takeFive.pipe(take(5)).subscribe((value: number) => console.log(`Taking only first five emissions: ${value}`));

// concat
let firstObservable: Observable<number> = interval(1000).pipe(take(5));
let secondObservable: Observable<number> = interval(500).pipe(take(10));
firstObservable.pipe(
    concat(secondObservable)
).subscribe((value: number) => console.log(value));

// map
let hashtagify: Observable<Event> = fromEvent(document.querySelector('#hashtagify'), 'click');

hashtagify.pipe(
    map(() => {
        let toHashTag = <HTMLInputElement>document.querySelector('#tohashtag');
        return `#${toHashTag.value}`
    })
).subscribe((output: string) => console.log(output));