import { TestBed, async } from '@angular/core/testing';
import { Input } from '@angular/core';

import { AppComponent } from './app.component';
import { Component } from '@angular/core';

@Component({
  selector: 'app-instructors-gallery',
  template: ''
})
class InstructorGalleryStubComponent {
  @Input('thumbnails') value = null;
}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, InstructorGalleryStubComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
