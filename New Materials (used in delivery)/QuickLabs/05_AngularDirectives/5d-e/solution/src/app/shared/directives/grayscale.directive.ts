import { Directive, ElementRef, HostListener, Input, OnInit } from '@angular/core';

@Directive({
    selector: `[appGrayscale]`
})
export class GrayscaleDirective implements OnInit {

    @Input() appGrayscale = `100%`;

    constructor(private el: ElementRef) { }

    ngOnInit() {
        this.el.nativeElement.style.filter = `grayscale(${this.appGrayscale})`;
        // this.el.nativeElement.style.filter = `grayscale(100%)`;
        this.el.nativeElement.style.transition = `filter 1s`;
    }

    @HostListener('mouseenter') onMouseEnter() {
        this.el.nativeElement.style.filter = `grayscale(0%)`;
    }

    @HostListener('mouseleave') onMouseLeave() {
        this.el.nativeElement.style.filter = `grayscale(${this.appGrayscale})`;
        // this.el.nativeElement.style.filter = `grayscale(100%)`;
    }

}
