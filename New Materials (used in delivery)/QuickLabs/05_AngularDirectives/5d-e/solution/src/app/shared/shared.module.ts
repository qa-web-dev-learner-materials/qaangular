import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GrayscaleDirective } from './directives/greyscale.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [GrayscaleDirective],
  exports: [GrayscaleDirective]
})
export class SharedModule { }
