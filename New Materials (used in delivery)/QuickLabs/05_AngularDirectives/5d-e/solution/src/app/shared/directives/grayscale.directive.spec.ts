import { TestBed, ComponentFixture } from '@angular/core/testing';

import { GrayscaleDirective } from './grayscale.directive';
import { Component, DebugElement, ElementRef } from '@angular/core';
import { By } from '@angular/platform-browser';

@Component({
    template: `<img src='../../assets/images/adrian.png' appGrayscale="50%" />`
})
class TestGrayscaleComponent { }

describe('GrayscaleDirective', () => {
    it('should create an instance', () => {
        const element: ElementRef = null;
        const directive = new GrayscaleDirective(element);
        expect(directive).toBeTruthy();
    });
});

describe('Testing Directive: Grayscale', () => {
    let component: TestGrayscaleComponent;
    let fixture: ComponentFixture<TestGrayscaleComponent>;
    let imageEl: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [GrayscaleDirective, TestGrayscaleComponent]
        });
        fixture = TestBed.createComponent(TestGrayscaleComponent);
        component = fixture.componentInstance;
        imageEl = fixture.debugElement.query(By.css('img'));
        fixture.detectChanges();
    });

    it(`should become 0% grayscale on mouseenter and 50% on mouseleave`, () => {
        expect(imageEl.nativeElement.style.filter).toEqual(`grayscale(50%)`);
        imageEl.triggerEventHandler('mouseenter', null);
        fixture.detectChanges();
        expect(imageEl.nativeElement.style.filter).toEqual(`grayscale(0%)`);
        imageEl.triggerEventHandler('mouseleave', null);
        fixture.detectChanges();
        expect(imageEl.nativeElement.style.filter).toEqual(`grayscale(50%)`);
    });
});
