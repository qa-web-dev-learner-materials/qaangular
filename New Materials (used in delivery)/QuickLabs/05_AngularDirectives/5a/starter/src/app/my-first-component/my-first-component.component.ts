import { Component, OnInit } from '@angular/core';

import { users } from '../users';

@Component({
  selector: 'app-my-first-component',
  templateUrl: './my-first-component.component.html',
  styleUrls: ['./my-first-component.component.css']
})
export class MyFirstComponentComponent implements OnInit {

  public readonly paraText: string = `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Adipisci, harum.`;
  public readonly title: string = `My Data Binding Experiments`;
  public readonly myClasses: string = `red light underlined`;
  public readonly imgUrl: string = `../assets/QA_logo_blue.png`;
  public selected = false;
  public inputtedText = ``;
  public readonly currentClasses: object = {
    selected: false
  };
  public currentStyles: object = {};
  public viewPara = false;
  public users: {username: string, upvotes: number, downvotes: number}[] = users;
  public selectedUser: {username: string};

  constructor() {}

  ngOnInit() {
  }

  setCurrentStyles(): void {
    this.currentStyles = {
      backgroundColor: this.selected ? 'lightpink' : 'lightgreen' ,
      border: this.selected ? '5px solid red' : '5px solid green'
    };
  }

  handleVote(event): void {
    console.log(`A vote was made, it was recieved as: ${event}`);
    const votedUsers = this.users;
    for (const user of votedUsers) {
      if (user.username === this.selectedUser.username) {
        (event === 1) ? user.upvotes += event : user.downvotes -= event;
      }
    }
    this.users = votedUsers;
  }
}
