import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

import { MyFirstComponentComponent } from './my-first-component.component';

describe('MyFirstComponentComponent', () => {
    let component: MyFirstComponentComponent;
    let fixture: ComponentFixture<MyFirstComponentComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MyFirstComponentComponent],
            imports: [FormsModule]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MyFirstComponentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});

describe('DOM Testing', () => {
    let fixture: ComponentFixture<MyFirstComponentComponent>;
    let myFirstComponentDe: DebugElement;
    let myFirstComponentEl: HTMLElement;

    beforeEach(async () => {
        TestBed.configureTestingModule({
            declarations: [MyFirstComponentComponent],
            imports: [FormsModule]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(MyFirstComponentComponent);
        myFirstComponentDe = fixture.debugElement;
        myFirstComponentEl = myFirstComponentDe.nativeElement;
    });

    describe('Testing paragraphs', () => {
        it('should render 9 paragraphs', () => {
            const paragraphs = myFirstComponentEl.querySelectorAll('p');
            expect(paragraphs.length).toBe(9);
        });

        it('should have a paragraph with an id of normal and bound text', () => {
            fixture.detectChanges();
            const expectedText = `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Adipisci, harum.`;
            const paraNormal = myFirstComponentEl.querySelector('#normal');
            expect(paraNormal).toBeTruthy();
            expect(paraNormal.textContent).toContain(expectedText);
        });
        it('should have a paragraph with a CSS class of red', () => {
            fixture.detectChanges();
            const paraRedDe = myFirstComponentDe.queryAll(By.css('p.red'));
            const paraRed = myFirstComponentEl.querySelectorAll('p.red');
            expect(paraRed.length).toBe(1);
            expect(paraRedDe.length).toBe(1);
        });
        it('should have no text initially in the paragraph with id userReflection', () => {
            fixture.detectChanges();
            const paraUserReflection = myFirstComponentEl.querySelector('p#userReflection');
            expect(paraUserReflection.textContent).toBeFalsy();
        });
        it('should have a paragraph with an initial background of limegreen', () => {
            fixture.detectChanges();
            const paraLimegreen = myFirstComponentEl.querySelector('p[style="background-color: limegreen;"]');
            expect(paraLimegreen).toBeTruthy();
        });
        it('should have a paragraph with a deepskyblue background when selected is true', () => {
            fixture.componentInstance.selected = true;
            fixture.detectChanges();
            const paraLimegreen = myFirstComponentEl.querySelector('p[style="background-color: deepskyblue;"]');
            expect(paraLimegreen).toBeTruthy();
        });
    });

    describe('Image tests', () => {
        it('should have an image from ../assets/QA_logo_blue.png and alt text of QA Logo', () => {
            fixture.detectChanges();
            const imageUrl = `assets/QA_logo_blue.png`;
            const image = myFirstComponentEl.querySelector('img');
            expect(image.src).toContain(imageUrl);
            expect(image.alt).toContain(`QA Logo`);
        });
    });

    describe('Button click testing', () => {
        it('clicking the button should change the state of selected', () => {
            const button = myFirstComponentDe.query(By.css('button'));
            fixture.detectChanges();
            expect(fixture.componentInstance.selected).toBeFalsy();
            button.triggerEventHandler('click', null);
            fixture.detectChanges();
            expect(fixture.componentInstance.selected).toBeTruthy();
            button.triggerEventHandler('click', null);
            fixture.detectChanges();
            expect(fixture.componentInstance.selected).toBeFalsy();
        });
    });
});
