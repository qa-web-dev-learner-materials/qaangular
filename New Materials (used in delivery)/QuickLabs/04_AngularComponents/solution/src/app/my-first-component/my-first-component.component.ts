import { Component, OnInit } from '@angular/core';

import { users } from '../users';

@Component({
  selector: 'app-my-first-component',
  templateUrl: './my-first-component.component.html',
  styleUrls: ['./my-first-component.component.css']
})
export class MyFirstComponentComponent implements OnInit {

  public readonly paraText: string = `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Adipisci, harum.`;
  public readonly title: string = `My Data Binding Experiments`;
  public readonly myClasses: string = `red light underlined`;
  public readonly imgUrl: string = `../assets/QA_logo_blue.png`;
  public selected = false;
  public inputtedText = ``;

  constructor() { }

  ngOnInit() {
  }
}
