export const users: {username: string, upvotes: number, downvotes: number}[] = [
    {
        username: 'Chris',
        upvotes: 0,
        downvotes: 0
      },
      {
        username: 'Edsel',
        upvotes: 0,
        downvotes: 0
      },
      {
        username: 'David',
        upvotes: 0,
        downvotes: 0
      },
      {
        username: 'Ed',
        upvotes: 0,
        downvotes: 0
      }
];
