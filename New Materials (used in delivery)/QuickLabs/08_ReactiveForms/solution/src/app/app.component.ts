import { Component } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Reactive Forms';
  genders = ['Female', 'Male'];

  // Up to the end of section 3
  // firstname = new FormControl('');
  // surname = new FormControl('');
  // age = new FormControl(null);
  // gender = new FormControl('');

  // Section 4
  // userForm = new FormGroup({
  //   firstname: new FormControl(''),
  //   surname: new FormControl(''),
  //   // Section 6
  //   address: new FormGroup({
  //     houseNo: new FormControl(''),
  //     street: new FormControl(''),
  //     city: new FormControl(''),
  //     postcode: new FormControl('')
  //   }),
  //   age: new FormControl(null),
  //   gender: new FormControl('')
  // });

  // Section 8 - Using FormBuilder

  constructor(private fb: FormBuilder) { }

  userForm = this.fb.group({
    firstname: [''],
    surname: [''],
    address: this.fb.group({
      houseNo: [''],
      street: [''],
      city: [''],
      postcode: ['']
    }),
    age: [null],
    gender: ['']
  });
}
