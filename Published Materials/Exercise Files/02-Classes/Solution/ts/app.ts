// Class definitions

class Course {
    description: string;

    constructor(
        public title: string,
        public id: string,
        public price: number,
    ) { }
}

class Instructor {
    constructor(
        public firstname: string,
        public lastname: string,
        public courses: Array<Course>
    ) { }
}

abstract class QAEvent {
    constructor(
        protected _course: Course,
        protected _instructor: Instructor,
        protected _date: string,
        protected _capacity: number,
        protected _delegates: number
    ) { }

    get course(): Course {
        return this._course;
    }

    get instructor(): Instructor {
        return this._instructor;
    }

    get date(): string {
        return this._date;
    }

    get capacity(): number {
        return this._capacity;
    }

    get delegates(): number {
        return this._delegates;
    }
}

class CBLEvent extends QAEvent {
    constructor(
        protected _course: Course,
        protected _instructor: Instructor,
        protected _date: string,
        protected _location: string,
        protected _capacity: number,
        protected _delegates: number
    ) {
        super(_course, _instructor, _date, _capacity, _delegates);
    }

    get location(): string {
        return this._location;
    }
}

class RAEvent extends QAEvent {
    constructor(
        protected _course: Course,
        protected _instructor: Instructor,
        protected _date: string,
        protected _vm: boolean,
        protected _capacity: number,
        protected _delegates: number
    ) {
        super(_course, _instructor, _date, _capacity, _delegates);
    }

    get vm(): boolean {
        return this._vm;
    }
}

// object instantiation

let theCourse = new Course("Programming with TypeScript", "QATYPESCRIPT", 399999);
theCourse.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus condimentum ligula, a mattis tellus. Vestibulum rutrum, purus eget malesuada ultricies, leo ante laoreet ex, ac rutrum ante est posuere metus. Pellentesque semper eros id mauris placerat, nec placerat metus ultrices. Curabitur eget efficitur dolor. Integer condimentum rutrum mi sit amet euismod. Vestibulum efficitur finibus nunc. Pellentesque pellentesque eleifend diam at convallis. Sed volutpat commodo justo ut fermentum. In aliquet ipsum eget purus finibus, ut tempor mauris vestibulum.";
let theInstructor = new Instructor('Chris', 'Bruford', [theCourse]);
let theQAEvent = new CBLEvent(theCourse, theInstructor, "6th March 2017", "IH", 12, 5);
let anotherEvent = new CBLEvent(theCourse, theInstructor, "18th April 2017", "MS", 7, 2);
let remoteEvent = new RAEvent(theCourse,theInstructor,"21st May 2017",true,4,3);

// take an array of QAEVent objects and populate the webpage with tables

createPricingTables([theQAEvent, anotherEvent, remoteEvent]);

function createPricingTables(events: Array<QAEvent>): void {
    let container = document.querySelector('[data-qa-courses]');

    events.forEach(event => {
        let pricingTable = document.createElement("table");

        let courseRow = pricingTable.appendChild(document.createElement("tr"));
        let dateRow = pricingTable.appendChild(document.createElement("tr"));
        let instructorRow = pricingTable.appendChild(document.createElement("tr"));
        let locationRow = pricingTable.appendChild(document.createElement("tr"));
        let delegatesRow = pricingTable.appendChild(document.createElement("tr"));
        let capacityRow = pricingTable.appendChild(document.createElement("tr"));


        courseRow.innerHTML = `<td>Course</td><td>${event.course.title}</td>`;
        dateRow.innerHTML = `<td>Date</td><td>${event.date}</td>`;
        instructorRow.innerHTML = `<td>Instructor</td><td>${event.instructor.firstname} ${event.instructor.lastname}</td>`;
        delegatesRow.innerHTML = `<td>Delegates</td><td>${event.delegates.toString()}</td>`;
        capacityRow.innerHTML = `<td>Capacity</td><td>${event.capacity.toString()}</td>`;

        container.appendChild(pricingTable);
    })
}

