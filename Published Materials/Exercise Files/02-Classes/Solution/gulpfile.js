var gulp = require('gulp');
var ts = require('gulp-typescript');
var merge = require('merge2');
var browserSync = require('browser-sync');
var tsProject = ts.createProject('tsconfig.json');
 
gulp.task('scripts', function() {
    var tsResult = gulp.src('./ts/*.ts')
        .pipe(tsProject());
 
    return merge([ // Merge the two output streams, so this task is finished when the IO of both operations is done. 
        tsResult.dts.pipe(gulp.dest('./definitions')),
        tsResult.js.pipe(gulp.dest('./js'))
    ]);
});
 
gulp.task('watch', function() {
    gulp.watch('./ts/*.ts', ['scripts', browserSync.reload]);
    gulp.watch('./*.html', browserSync.reload);
});

gulp.task('serve',['scripts','watch'], function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});
